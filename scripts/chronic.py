'''
Copyright (c) 2016 Rob "N3X15" Nelson <nexisentertainment@gmail.com>
'''
import os
import time
from datetime import datetime, timedelta

import psutil

import yaml
from buildtools import cli, log, os_utils
from buildtools.config import YAMLConfig
from win32api import GetLastInputInfo, GetTickCount

CHRONIC_VERSION = '0.0.1'
ONE_MINUTE = 60000

APPS = []
EVENTS = []
PERIODS = []
TIME_LOGGED = {}


def getNow():
    now = datetime.utcnow()
    return now.replace(microsecond=0)


class MatcherBase(object):

    def Matches(self, proc):
        return


class Matcher(object):
    all = {}

    def __init__(self, key):
        self.key = key

    def __call__(self, c):
        log.info('Adding %s as Matcher %r', c.__name__, self.key)
        Matcher.all[self.key] = c
        return c


@Matcher('commandline-contains')
class CommandLineMatcher(MatcherBase):

    def __init__(self, config):
        self.items = config

    def Matches(self, proc):
        cl = proc.cmdline()
        for item in self.items:
            if item in cl:
                return True
        return False


def getDuration(delta):
    o = ''
    s = delta.total_seconds()
    days, s = divmod(s, 86400)
    hours, s = divmod(s, 3600)
    minutes, seconds = divmod(s, 60)
    if days > 0:
        o += '{}d'.format(int(days))
    if hours > 0:
        o += '{}h'.format(int(hours))
    if minutes > 0:
        o += '{}m'.format(int(minutes))
    if seconds > 0:
        o += '{}s'.format(int(seconds))
    return o


class AppTracker(object):

    def __init__(self, name, cfg):
        self.imagename = cfg['image']
        self.name = name

        self.process = None
        self.was_running = None
        self.started_at = None
        self.first_check = True

        self.ignore = []
        if 'ignore' in cfg:
            for matcherID, matcherCfg in cfg['ignore'].iteritems():
                matcher = Matcher.all[matcherID]
                m = matcher(matcherCfg)
                self.ignore.append(m)

    def tick(self):
        now = getNow()
        is_run = self.is_running()
        if is_run != self.was_running or self.first_check:
            if is_run:
                if self.first_check:
                    appendEvent('NOTE: {name} is already running as of {time}UTC'.format(name=self.name, time=now.isoformat()))
                else:
                    appendEvent('STARTED {name} at {time}UTC'.format(name=self.name, time=now.isoformat()))
                self.started_at = now
            else:
                if not self.first_check:
                    self.stopped(now)
        self.was_running = is_run
        self.first_check = False

    def stopped(self, now, punchout=False):
        global TIME_LOGGED
        self.process = None
        delta = (now - self.started_at)
        if not punchout:
            appendEvent('STOPPED {name} at {time}UTC, worked for {duration}'.format(name=self.name, time=now.isoformat(), duration=getDuration(delta)))
        TIME_LOGGED[self.name] = TIME_LOGGED.get(self.name, timedelta()) + delta
        self.started_at = None

    def is_ignored(self, proc):
        for rule in self.ignore:
            if rule.Matches(proc):
                return True
        return False

    def is_running(self):
        if self.process is None:
            for proc in psutil.process_iter():
                try:
                    if proc.name() == self.imagename:
                        if self.is_ignored(proc):
                            continue
                        self.process = proc
                except psutil.AccessDenied:
                    continue
        if self.process is None:
            return False
        try:
            if self.process.status() == psutil.STATUS_RUNNING:
                return True
        except psutil.NoSuchProcess:
            return False

        return False


def appendEvent(event):
    EVENTS.append(event)
    log.info(event)


def constructDelta(loader, node):
    seconds = int(loader.construct_scalar(node))
    return timedelta(seconds=seconds)


def representDelta(dumper, data):
    return dumper.represent_scalar(u'!delta', u'%d' % data.total_seconds())


def archiveTimecard(filename):
    dirname = os.path.dirname(filename)
    basename, ext = os.path.splitext(os.path.basename(filename))
    with open(filename, 'r') as f:
        timecard_data = yaml.load(f)
    start = timecard_data['start']
    newfilename = os.path.join(dirname, 'Timecard-{M}-{D}-{Y}.yml'.format(D=start.day, M=start.month, Y=start.year))
    os_utils.single_copy(filename, newfilename, verbose=True)
    report_filename = os.path.join(dirname, 'timesheet.txt')
    new_report_filename = os.path.join(dirname, 'Timesheet-{M}-{D}-{Y}.txt'.format(D=start.day, M=start.month, Y=start.year))
    os_utils.single_copy(report_filename, new_report_filename, verbose=True)

yaml.add_representer(timedelta, representDelta)
yaml.add_constructor(u'!delta', constructDelta)


def main():
    global PERIODS, EVENTS, APPS, TIME_LOGGED
    TIME_START = SESSION_START = TIMESHEET_START = getNow()
    IDLE_START = None
    WAS_IDLE = False
    log.info('Chronic v%s - http://gitlab.com/N3X15/Chronic', CHRONIC_VERSION)
    log.info('-' * 60)
    config = YAMLConfig('chronic.yml', {'apps': {}})
    for appName, appData in config.get('apps', {}).iteritems():
        APPS.append(AppTracker(appName, appData))
    timecard_path = os.path.abspath(config.get('settings.paths.timecard', './timecard.yml'))
    if os.path.isfile(timecard_path):
        if cli.getInputChar('Timecard exists.  Reopen/Archive?', 'ra', 'a') == 'a':
            log.info('Archiving...')
            archiveTimecard(timecard_path)
        else:
            log.info('Reading %s...', timecard_path)
            with open(timecard_path, 'r') as f:
                timecard_data = yaml.load(f)
                EVENTS = timecard_data['event_log']
                TIME_LOGGED = timecard_data['app_time']
                for app, tdelta in TIME_LOGGED.iteritems():
                    log.info('%s - %s', app, getDuration(tdelta))
                PERIODS = timecard_data['periods']
                TIMESHEET_START = timecard_data['start']
    appendEvent('PUNCH-IN at {time}UTC'.format(time=getNow().isoformat()))
    while True:
        now = getNow()
        try:
            for app in APPS:
                app.tick()
            idle_ticks = int(GetTickCount() - GetLastInputInfo())
            is_idle = idle_ticks >= ONE_MINUTE
            if is_idle != WAS_IDLE:
                if is_idle:
                    delta = (now - TIME_START) - timedelta(minutes=1)
                    appendEvent('IDLE at {time}UTC, worked for {duration}'.format(time=now.isoformat(), duration=getDuration(delta)))
                    TIME_LOGGED['ALL'] = TIME_LOGGED.get('ALL', timedelta()) + delta
                    PERIODS.append([TIME_START, now])
                    TIME_START = None
                    IDLE_START = now
                else:
                    delta = (now - IDLE_START)
                    appendEvent('WORKING at {time}UTC, idle for {duration}'.format(time=now.isoformat(), duration=getDuration(delta)))
                    TIME_START = now
                    IDLE_START = None
            WAS_IDLE = is_idle
            time.sleep(1)
        except KeyboardInterrupt:
            appendEvent('PUNCH-OUT at {time}UTC, entire session was {duration}'.format(time=now.isoformat(), duration=getDuration(now - SESSION_START)))
            if not WAS_IDLE:
                delta = (now - TIME_START)
                appendEvent('PUNCH-OUT at {time}UTC, worked for {duration}'.format(time=now.isoformat(), duration=getDuration(delta)))
                TIME_LOGGED['ALL'] = TIME_LOGGED.get('ALL', timedelta()) + delta
                PERIODS.append([TIME_START, now])
                TIME_START = None
            for app in APPS:
                if app.was_running:
                    app.stopped(now, True)
            with open(timecard_path, 'w') as f:
                structure = {
                    "app_time": TIME_LOGGED,
                    "periods": PERIODS,
                    "event_log": EVENTS,
                    "start": TIMESHEET_START,
                }
                yaml.dump(structure, f, default_flow_style=False)
            with open('timesheet.txt', 'w') as f:
                f.write('Timesheet Report - Compiled by Chronic v{} <http://gitlab.com/N3X15/Chronic>\n'.format(CHRONIC_VERSION))
                f.write('\nNOTE: All times are in UTC.\n')
                f.write('\n# TIMESHEET PERIOD\n\nNote: Does include idle periods.\n\n')
                f.write('Start....: {}UTC\n'.format(TIMESHEET_START.isoformat()))
                f.write('End......: {}UTC\n'.format(now.isoformat()))
                f.write('Duration.: {}\n'.format(getDuration(now - TIMESHEET_START)))
                f.write('\n# WORK PERIODS\n\n')
                i = 0
                for start, end in PERIODS:
                    i += 1
                    f.write('#{}: {} - {}\n'.format(i, start, end))
                f.write('\nNon-Idle Work Time: {}\n'.format(getDuration(TIME_LOGGED['ALL'])))
                f.write('\n# EVENT LOG\n\n')
                i = 0
                for event in EVENTS:
                    i += 1
                    f.write('#{}: {}\n'.format(i, event))
                f.write('\n# TOTAL APPLICATION RUNTIME\n\nNOTE: Does not count time apps were open outside of tracked time period\n\n')
                for appID, appDelta in TIME_LOGGED.iteritems():
                    if appID != 'ALL':
                        f.write(' * {}: {}\n'.format(appID, getDuration(appDelta)))
            break

if __name__ == '__main__':
    main()
