# chronic

A simple timeclock for freelancers.  Runs on Windows.  Linux and Mac support planned.

# Installing

Requires my python-build-tools and Python 2.7.

# Running

1. Open `chronic.yml.dist`
2. Add or remove apps as needed.
3. Rename to `chronic.yml`
4. `python scripts/chronic.py` to clock in.
5. CTRL+C to clock out.
6. Review `timesheet.txt`, if you wish.

# Example Timesheet

```
Timesheet Report - Compiled by Chronic v0.0.1 <http://gitlab.com/N3X15/Chronic>

NOTE: All times are in UTC.

# TIMESHEET PERIOD

Note: Does include idle periods.

Start....: 2016-02-04T09:19:16UTC
End......: 2016-02-04T09:26:08UTC
Duration.: 6m52s

# WORK PERIODS

#1: 2016-02-04T09:19:16 - 2016-02-04T09:23:15
#2: 2016-02-04T09:23:52 - 2016-02-04T09:26:08

Non-Idle Work Time: 6m15s

# EVENT LOG

#1: NOTE: Unity is already running as of 2016-02-04T09:19:16UTC
#2: NOTE: Atom is already running as of 2016-02-04T09:19:16UTC
#3: STARTED MonoDevelop at 2016-02-04T09:21:09UTC
#4: STOPPED Atom at 2016-02-04T09:21:28UTC, worked for 2m12s
#5: STARTED Notepad++ at 2016-02-04T09:21:48UTC
#6: STOPPED Notepad++ at 2016-02-04T09:22:12UTC, worked for 24s
#7: IDLE at 2016-02-04T09:23:15UTC, worked for 3m59s
#8: WORKING at 2016-02-04T09:23:52UTC, idle for 37s
#9: STARTED Atom at 2016-02-04T09:24:09UTC
#10: PUNCHOUT at 2016-02-04T09:26:08UTC, entire session was 6m52s
#11: PUNCHOUT at 2016-02-04T09:26:08UTC, worked for 2m16s

# TOTAL APPLICATION RUNTIME

NOTE: Does not count time apps were open outside of tracked time period

 * MonoDevelop: 4m59s
 * Notepad++: 24s
 * Unity: 6m52s
 * Atom: 4m11s

```
